<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Anunturi gratuite</title>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">

    <?php include "config.php";
    include "functions.php";
    include "header1.php";
    include "header2.php";
    $table = 'anunturi';
    $selData['category'] = $_GET['cat'];
    $sortBy = 'price';
    $list = dbSelect($table, $selData, null, 0,null, $sortBy);
    listare($list); ?>

</div>

</body>
</html>