<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Anunturi gratuite</title>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">

    <?php include "config.php";
    include "functions.php";
    include "header1.php";
    $eroare = $_GET['error'];
    ?>
    <div class="cont float-right">
        <div class="row">
           <div class="col">
               <h3>Intra in cont</h3>
               <form action="aut_cont.php" method="post">
                   <div class="form-group">
                       <input type="text" class="form-control" name="mail" placeholder="E-mail">
                   </div>
                   <div class="form-group">
                       <input type="password" class="form-control" name="parola" placeholder="Password">
                   </div>
                   <button type="submit" class="btn btn-primary">Intra in cont</button>
               </form>
               <?php echo $eroare; ?>
           </div>
           <div class="col">
               <h3>Creaza un cont</h3>
               <form action="add_cont.php" method="post">
                   <div class="form-group">
                       <input type="text" class="form-control" name="user" placeholder="Username">
                   </div>
                   <div class="form-group">
                       <input type="text" class="form-control" name="mail" placeholder="E-mail">
                   </div>
                   <div class="form-group">
                       <input type="password" class="form-control" name="parola" placeholder="Password">
                   </div>
                   <button type="submit" class="btn btn-primary">Creaza cont</button>
               </form>
           </div>
        </div>

    </div>

</div>

</body>
</html>