<?php
include "config.php";

function menu($men) {
$i=0; ?>
<div class="row c1">
<?php foreach ($men as $line) { ?>
        <div class="col-3">
            <div class="row">
                <div class="col-4">
                   <a href="<?php echo $line['url']; ?>"><img height=50px src="<?php echo $line['image']; ?>"></a>&nbsp
                </div>
                <div class="col-8">
                <a href="<?php echo $line['url']; ?>"><?php echo $line['name']; ?></a>
                </div>
            </div>
        </div>
    <?php $i++;
    if (($i % 4 ) == 0) {
        echo "</div><br>";
        echo "<div class=\"row c1\">";
    }

    }
    echo "</div>";
}

function promo($prom) {

    $i=0; ?>
    <div class="row c1">
        <?php foreach ($prom as $line) { ?>
        <div class="col-3">
            <a href="disp_item.php?id=<?php echo $line['id']; ?>"><img height=130px src="<?php echo $line['image']; ?>"></a><br>
            <a href="disp_item.php?id=<?php echo $line['id']; ?>"><?php echo $line['title_an']; ?></a><br>
            <?php
            echo $line['price']." ".$line['valuta']; ?><br>
        </div>
        <?php $i++;
        if (($i % 4 ) == 0) {
           echo "<br><br></div>";
           echo "<div class=\"row c1\">";
           }
        }
    echo"</div>";
}

function afisare($item) { ?>

        <br><hr><br>
        <div class="row">
            <div class="col-8">
                <img src="<?php echo $item[0]['image']; ?>">
                <h3><?php echo $item[0]['title_an']; ?></h3>
                <?php echo $item[0]['city']; ?>
                <br><br>
                <div class="row">
                    <?php
                       if ($item[0]['category'] == 'Auto') {
                           echo "<div class=\"col\">";
                              echo "Model"."<br>";
                              echo "An de fabricatie"."<br>";
                              echo "Capacitate motor"."<br>";
                           echo "</div>";
                           echo "<div class=\"col\">";
                               echo $item[0]['type_br']."<br>";
                               echo $item[0]['year_prod']."<br>";
                               echo $item[0]['cap_motor']." cm³"."<br>";
                           echo "</div>";
                           echo "<div class=\"col\">";
                               echo "Marca"."<br>";
                               echo "Culoare"."<br>";
                               echo "Rulaj"."<br>";
                           echo "</div>";
                           echo "<div class=\"col\">";
                               echo $item[0]['brand']."<br>";
                               echo $item[0]['color']."<br>";
                               echo $item[0]['rulaj']." km"."<br>";
                           echo "</div>";
                       } ?>

                    <?php
                       if ($item[0]['category'] == 'Imobiliare') {
                           echo "<div class=\"col\">";
                              echo "Suprafata utila"."<br>";
                              echo "Etaj"."<br>";
                           echo "</div>";
                           echo "<div class=\"col\">";
                               echo $item[0]['surface']."<br>";
                               echo $item[0]['floor']."<br>";
                           echo "</div>";
                           echo "<div class=\"col\">";
                               echo "An constructie"."<br>";
                           echo "</div>";
                           echo "<div class=\"col\">";
                                echo $item[0]['year_prod']."<br>";
                           echo "</div>";
                       } ?>

                </div>
                    <br><hr><br>
                       <?php echo "<pre>".$item[0]['description']."</pre>"; ?>
            </div>

            <div class="col-4">
                <h3> <?php echo $item[0]['price']." ".$item[0]['valuta']; ?></h3>
                <br><br>
                <div class="mesaj">
                <span><i class='fas fa-envelope' style="color:white"</span>&nbsp&nbsp
                <a href="new_mesaj.php?id=<?php echo $item[0]['user_id']; ?>"> Trimite mesaj </a>
                </div>
                <br>
                <div class="mesaj">
                <span><i class="fas fa-phone" style="color:white"</span>&nbsp&nbsp <?php echo $item[0]['phone']; ?>
                </div>
                <br>
                <div class="mesaj">
                <?php echo $item[0]['username']; ?>
                </div>
            </div>
        </div>
<?php }

function listare($res) {
    foreach ($res as $line) { ?>
        <div class="row lis">
           <div class="col-3">
        <a href="disp_item.php?id=<?php echo $line['id']; ?>"><img height=150px src="<?php echo $line['image']; ?>"></a>
           </div>
           <div class="col-7">
               <a href="disp_item.php?id=<?php echo $line['id']; ?>"><h3 style='color:#0098d0'><?php echo $line['title_an']; ?></h3></a>
               <br><br><br>
               <?php echo $line['city']."&nbsp &nbsp ".$line['act_an']; ?>
           </div>
           <div class="col-2 float-right">
                 <h3><?php echo $line['price']." ".$line['valuta'];?></h3>
           </div>
        </div>
    <?php
    }
}

function dbInsert($table, $data) {
    global $conn;
    $col = [];
    $values = [];
    foreach ($data as $key => $val) {
        $col[] = mysqli_real_escape_string($conn, $key);
        $values[] = "'".mysqli_real_escape_string($conn, $val)."'";
    }

    $strCol = implode(",",$col);
    $strVal = implode(",",$values);
    $sql = "INSERT INTO $table ($strCol) VALUES ($strVal)";
    $conn->query($sql);
}

function dbUpdate($table, $id, $data){
    global $conn;
    $sets = [];
    foreach ($data as $column => $value){
        $sets[]="`".mysqli_real_escape_string($conn, $column)."`='".mysqli_real_escape_string($conn, $value)."'";
    }
    $sqlSets = implode(',', $sets);
    $sql = "UPDATE $table SET $sqlSets id=".intval($id);
    $conn->query($sql);
        return mysqli_affected_rows($conn)>0;
}

function dbDelete($table, $id){
    global $conn;
    $sql = "DELETE FROM $table WHERE id=".intval($id);
    $conn->query($sql);
    return mysqli_affected_rows($conn)>0;
}

 function dbSelect($table, $filters=null, $likeFilters=null, $offset=0, $limit=null,  $sortBy=null, $sortDirection='ASC'){
    global $conn;
    $sql = "SELECT * FROM $table";

    if (($filters != null)||($likeFilters != null)){
         $sets = [];
         if ($filters != null) {
         foreach ($filters as $column => $value){
            if ($value != null) {
               $sets[] = mysqli_real_escape_string($conn, $column)."='".mysqli_real_escape_string($conn, $value)."'";
               }
            }
        }
         if ($likeFilters != null) {
        foreach ($likeFilters as $column => $value){
             if ($value != null) {
                $sets[] = mysqli_real_escape_string($conn, $column)." LIKE '%".mysqli_real_escape_string($conn, $value)."%'";
                }
            }
        }
        $sql.= ' WHERE '.implode(' AND ', $sets);
    }

    if ($sortBy != null) {
        $sql.= ' ORDER BY '.mysqli_real_escape_string($conn, $sortBy).' '.mysqli_real_escape_string($conn, $sortDirection);
    }

    if ($limit != null){
        $sql.= ' LIMIT '.intval($offset).','.intval($limit);
    }

    $result = mysqli_query($conn, $sql);

    if (!$result){
        die("SQL error: " . mysqli_error($conn)." SQL:".$sql);
    }

    return $result->fetch_all(MYSQLI_ASSOC);
}








