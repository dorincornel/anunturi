<?php
session_start();
include "config.php";
include "functions.php";
$addData = [];
$table = 'anunturi';

$addData['category'] = $_POST["categ"]; //categoria
$addData['title_an'] = $_POST["titlu"]; //titlul anuntului
$addData['description'] = $_POST["descriere"]; //destinatia anuntului
$addData['brand'] = $_POST["marca"];  //marca autoturismului
$addData['type_br'] = $_POST["model"];
$addData['cap_motor'] = $_POST["capmot"];
$addData['color'] = $_POST["culoare"];
$addData['year_prod'] = $_POST["anul"];
$addData['rulaj'] = $_POST["rulaj"];
$addData['rooms'] = $_POST["camere"];
$addData['surface'] = $_POST["suprafata"];
$addData['floor'] = $_POST["etaj"];
$addData['act_an'] = $_POST["actiune"];
$addData['price'] = $_POST["pret"];
$addData['valuta'] = $_POST["valuta"];
$addData['promo'] = $_POST["promo"];
$addData['phone'] = $_POST["telefon"];
$addData['city'] = $_POST["oras"];
$addData['user_id'] = $_SESSION['user_id'];
$addData['username'] = $_SESSION['username'];
$addData['image'] = "images/id".$addData['user_id']."_".basename($_FILES["fupload"]["name"]);

dbInsert($table,$addData);

$target = "images/id".$addData['user_id']."_".basename($_FILES["fupload"]["name"]);
$source = $_FILES["fupload"]["tmp_name"];
move_uploaded_file($source , $target);

header("Location: new_anunt.php");

?>
