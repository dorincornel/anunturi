<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Anunturi gratuite</title>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<?php
include "header11.php";
if ($_SESSION['login'] == true) { ?>

<div class="container">
    <h3>Introducere anunt nou</h3>
    <form action="add_anunt.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="categorie">Selectati categoria:</label>
            <select class="form-control" id="categorie" name="categ">
                <option>Auto</option>
                <option>Imobiliare</option>
                <option>Electronice si electrocasnice</option>
                <option>Sport</option>
                <option>Casa si gradina</option>
                <option>Locuri de munca</option>
                <option>Moda si frumusete</option>
                <option>Animale de companie</option>
                <option>Cazare</option>
                <option>Mama si copilul</option>
                <option>Servicii</option>
            </select>

        <div class="form-group">
            <label for="ta">Titlu anunt:</label>
            <input type="text" class="form-control" id="ta" name="titlu">
        </div>
        <div class="form-group">
            <label for="des">Descriere anunt:</label>
            <textarea class="form-control" id="des" rows="3" name="descriere"></textarea>
        </div>
        <div class="form-group">
            <label for="ma">Marca:</label>
            <input type="text" class="form-control" size="30" id="ma" name="marca">
        </div>
        <div class="form-group">
            <label for="mod">Modelul:</label>
            <input type="text" class="form-control" size="20" id="mod" name="model">
        </div>
        <div class="form-group">
            <label for="col">Culoarea:</label>
            <input type="text" class="form-control" size="15" id="col" name="culoare">
        </div>
        <div class="form-group">
            <label for="an">Anul de fabricatie:</label>
            <input type="text" class="form-control" size="20" id="an" name="anul">
        </div>
        <div class="form-group">
            <label for="rul">Rulaj:</label>
            <input type="text" class="form-control" size="10" id="rul" name="rulaj">
        </div>
        <div class="form-group">
            <label for="motor">Capacitate motor:</label>
            <input type="text" class="form-control" size="10" id="motor" name="capmot">
        </div>
        <div class="form-group">
            <label for="camere">Numar de camere:</label>
            <input type="text" class="form-control" size="4" id="camere" name="camere">
        </div>
        <div class="form-group">
            <label for="supr">Suprafata:</label>
            <input type="text" class="form-control" size="10" id="supr" name="suprafata">
        </div>
        <div class="form-group">
            <label for="et">Etaj:</label>
            <input type="text" class="form-control" size="3" id="et" name="etaj">
        </div>
        <div class="form-group">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="actiune" value="Vinde"> Vinde &nbsp &nbsp &nbsp
                <input type="radio" class="form-check-input" name="actiune" value="Inchiriaza"> Inchiriaza &nbsp &nbsp &nbsp
            </label>
        </div>
        <div class="form-group">
            <label for="price">Pretul:</label>
            <input type="text" class="form-control" size="20" id="price" name="pret">
        </div>
        <div class="form-group">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="valuta" value="EUR"> EURO &nbsp &nbsp &nbsp
                <input type="radio" class="form-check-input" name="valuta" value="RON"> LEI &nbsp &nbsp &nbsp
            </label>
        </div>
        <div class="form-group">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="promo" value="Y"> Promovez &nbsp &nbsp &nbsp
                <input type="radio" class="form-check-input" name="promo" value="N"> NU &nbsp &nbsp
            </label>
        </div>
        <div class="form-group">
            <label for="tel">Telefon:</label>
            <input type="text" class="form-control" size="20" id="tel" name="telefon">
        </div>
        <div class="form-group">
            <label for="loc">Localitatea:</label>
            <input type="text" class="form-control" size="30" id="loc" name="oras">
        </div>
        <div class="form-group">
            <label for="fileUp">Nume fisier imagine:</label>
            <input type="hidden" class="form-control" name="MAX_FILE_SIZE" value="2000000">
            <input type="file" class="form-control" name="fupload" id="fileUp">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<?php }
else {
    session_destroy();
    header("Location: myaccount.php?error=Nu sunteti logat");
} ?>

</body>
</html>
