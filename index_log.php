<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Anunturi gratuite</title>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">

    <?php include "config.php";
    include "functions.php";
    include "header11.php";
    include "header2.php";
    include "main_menu.php" ?>
    <div class="row">
        <h3>Anunturi promovate</h3>
    </div>
    <?php include "promo.php"; ?>

</div>

</body>
</html>